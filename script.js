const moviesContainer = document.querySelector(".movies-container");

fetch(" https://ajax.test-danit.com/api/swapi/films")
  .then(response => response.json())
  .then(response => {
    response.forEach(movie => {

      const movieTitle = document.createElement("h2");
      const movieEpisode = document.createElement("p");
      const movieCrawl = document.createElement("p");
      const characters = document.createElement("p");
      const loader = document.createElement("div");
      loader.classList.add("loader");

      movieTitle.textContent = movie.name;
      movieEpisode.textContent = `Episode: ${movie.episodeId}`;
      movieCrawl.textContent = `Opening Crawl: ${movie.openingCrawl}`;

      const fetchCharacter = url => fetch(url).then(character => character.json()).catch(err => console.log(err));

      // This variant uses Promise.all to fetch all characters at once and then render their names
      Promise.all(movie.characters.map(fetchCharacter))
        .then(charactersList => {
          charactersList.forEach(character => {
            characters.textContent += `${character.name}, `;
            loader.remove();
          })
        }).catch(err => console.log(err));

      // This variant provides with gradual downloading of the characters
      /*movie.characters.forEach(character => {
        fetch(character).then(response => response.json()).then(char => {
          characters.textContent += `${char.name}, `;
        })
      })*/

      moviesContainer.appendChild(movieTitle);
      moviesContainer.appendChild(movieEpisode);
      moviesContainer.appendChild(movieCrawl);
      moviesContainer.appendChild(characters);
      moviesContainer.append(loader);
    });
  }).catch(err => console.log(err));



